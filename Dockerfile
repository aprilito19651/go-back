############################
#STEP 1 build executable binary
############################
FROM golang:alpine AS builder
# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git
#WORKDIR $GOPATH/src/myapp/
WORKDIR /app
COPY go/ .
# Fetch dependencies.
# Using go get.
RUN go get -d -v && go get github.com/enescakir/emoji
# Build the binary.
RUN CGO_ENABLED=0 GOOS=linux go build -v -o main
###########################
# STEP 2 build a small image
##########################
FROM scratch 
 #alpine:latest
# Copy our static executable.
#RUN apk add bash
COPY --from=builder /app/main /main
#ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/
#WORKDIR /
CMD chmod +x main
#EXPOSE 8000
ENTRYPOINT ["/main"]
